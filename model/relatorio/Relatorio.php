<?php
/**
 * Description of Relatorio
 *
 * @author Allan Vigiano
 * 
 * Baseado na classe criada pelo Willian 
 * http://www.devwilliam.com.br/php/biblioteca-mpdf-gerar-relatorios-em-php
 */
require_once "model/mpdf60/mpdf.php";
class Relatorio {
    private $css  = null;  
    private $titulo = null; 
    private $formato = "A4-P";
        //'A4-P' -> Retrato
        //'A4-L' -> Paisagem
    
    function setFormato($formato) {
        $this->formato = $formato;
    }

    public function __construct($css, $titulo) {
        
        $this->titulo = $titulo;  
        $this->setCSS($css);  
    }
    
    public function BuildPDF(){
        
        $this->pdf = new mPDF('utf-8', $this->formato);
        $this->pdf->WriteHTML($this->css, 1);
        
        /*
         * Footer e Header foram baseados no método 2 
         * (http://mpdf1.com/manual/index.php?tid=311)
         * 
         */
        $this->pdf->SetHTMLHeader($this->makeHeader());  
        $this->pdf->SetHTMLFooter($this->makeFooter());
        $this->pdf->WriteHTML($this->makeHeaderPrimeiraPagina()); // imprime título do relatório no pdf

    }
    
    
    public function exibir($name, $acao = "D") {  
        return $this->pdf->Output($name, $acao);
        /*
         * I: send the file inline to the browser. The plug-in is used if available. The name given by filename is used when one selects the "Save as" option on the link generating the PDF.
         * D: send to the browser and force a file download with the name given by filename.
         * F: save to a local file with the name given by filename (may include a path).
         * S: return the document as a string. filename is ignored.
         */
    }
    
    public function setCSS($file){  
        if (file_exists($file)) {
            $this->css = file_get_contents($file);  
        }
        
    }
    /*
     * makeHeader() e makeFooter() são funções para gerar o header e o footer
     */
    protected function makeHeader(){  
       $data = date('d/m/Y');
       $hora = date('H:i:s');
       $string = "<div id='header'> Emissão:  $data  $hora</div>" ;  
       return $string;  
    }
    protected function makeFooter(){  
       $retorno = 
               "<div id='footer'>Página: {PAGENO}/{nb}
                </div>";  
        return $retorno;  
    }
    
    protected function makeHeaderPrimeiraPagina(){
        $string = "
                <table id='header-primeira-pagina'>  
                    <tr>  
                        <td>
                            VEJA
                        </td>     
                    </tr>
                    <tr>
                        <td id='titulo'>$this->titulo</td>
                    </tr>
                </table>                
        ";

        return $string;
    }     
    
    public function setBody($html) {        
        $this->pdf->WriteHTML($html, 2);
    }
}
